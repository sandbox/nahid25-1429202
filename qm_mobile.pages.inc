<?php

/**
 * page callback function for module path
 */
function qm_mobile_home() {

  $content = "";
  global $language;
  $lang = $language->language;
  //
  //	  drupal_add_css(drupal_get_path('module', 'qm_mobile') . '/qm_mobile.css');
  //    drupal_add_js(drupal_get_path('module', 'qm_mobile') . '/qm_mobile.js');

  $content = l("Home", 'mobile');
  $content .= " | ";
  $content .= "<div id='test'>";
  $content .= l("Free Download", 'mobile/download/category');
  $content .= "</div>";
  $content .= "<br />";

  $query = db_select('node', 'n')
    ->fields('n', array('title','nid','status', 'created'));
  $query->addJoin('INNER', 'field_data_field_publication_category', 'c', 'n.nid = c.entity_id');
  $query->condition('n.sticky', 1);
  $query->condition('n.status', 1);
  $query->condition('n.language', 'en');
  $query->orderBy('n.created', 'DESC');

  $result = $query->execute();


  $content .= "<div id='featured_download'>";
  $content .= "<h3>Featured Downloads</h3>";
  $content .= "<ul>";
  foreach($result as $data ) {
    $content .= "<li>" . l($data->title, 'mobile/download/node/' . $data->nid) . "</li>";
  }
  $content .= "</ul>";
  $content .= "</div>";
  
  echo $content;
}



function qm_mobile_download_category_list() {

  $content = "";

  $query = db_select('taxonomy_term_data', 't')
    ->fields('t', array('tid','name'));
  $query->addJoin('INNER', 'field_data_field_publication_category', 'c', 't.tid = c.field_publication_category_tid'); // join field table
  $query->distinct();
  $query->orderBy('name', 'ASC');

  $result = $query->execute();

  $content .= "<ul>";
  foreach($result as $data ) {
    $content .= "<li>" . l($data->name, 'mobile/download/category/' . $data->tid) . "</li>";
  }
  $content .= "</ul>";

  echo $content;


}


function qm_mobile_download_by_category($tid) {

  $content = "";

  $query = db_select('node', 'n')
    ->fields('n', array('title','nid','status'));
  $query->addJoin('INNER', 'field_data_field_publication_category', 'c', 'n.nid = c.entity_id');
  $query->condition('c.field_publication_category_tid', $tid);
  $query->condition('n.status', 1);
  $query->condition('n.language', 'en');

  $result = $query->execute();


  $content = "<ul>";
  foreach($result as $data ) {
    $content .= "<li>" . l($data->title, 'mobile/download/node/' . $data->nid) . "</li>";
  }
  $content .= "</ul>";
  echo $content;

}

function qm_mobile_download_page($nid) {


//  $get_count = db_select('node_counter', 'n')
//    ->fields('n', array('totalcount', 'daycount'))
//    ->condition('n.nid', $nid)
//     ->execute();
//   $count = $get_count->fetchAssoc();
//
//  $node_counter = db_update('node_counter')
//      ->fields(array(
//                    'totalcount' => $count++
//               ))
//    ->condition('n.nid', $nid)
//      ->execute();

  $content = "";

  $query = db_select('file_managed', 'f')
    ->fields('f', array('fid','uri', 'filename', 'filemime', 'filesize'));
  $query->addJoin('INNER', 'field_data_field_publication_attachment', 'a', 'a.field_publication_attachment_fid = f.fid');
  $query->condition('a.entity_id', $nid);

  $result = $query->execute();


  $content = '<table cellpadding = "10" style = "border: 1px solid #aaa">';
  $content .= '<tr bgcolor="#bbeeff">';
  $content .= "<th>File</th>";
  $content .= "<th>Size</th>";
  $content .= "</tr>";

  foreach($result as $data ) {


    if ($data->filesize > 1048576){
      $size = number_format($data->filesize / 1048576, 2, '.', '') . " MB";
    }elseif ($data->filesize > 1024) {
      $size = number_format($data->filesize / 1024, 2, '.', '') . " KB";
    }else{
      $size = number_format($data->filesize, 2, '.', '') . " Bytes";
    }

    $content .= "<tr>";
    $content .= "<td>" . l($data->filename, file_create_url($data->uri)) . "</td>";
    $content .= "<td>" . $size . "</td>";
    $content .= "</tr>";
  }
  $content .= "</table>";
  
  echo $content;
}